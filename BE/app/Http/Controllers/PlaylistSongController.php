<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Songs;

class PlaylistSongController extends Controller
{
    public function displaySongs(){
        return DB::table('Playlist_Songs')->get();
    }
    public function store(Request $request){

    $newSongs = new Playlist_Songs();
    $newSongs->song_id = $request->song_id;
    $newSongs->playlist_id = $request->playlist_id;
    $newSongs->save();
    return $newSongs;
}
}
