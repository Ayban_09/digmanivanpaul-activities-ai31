<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlists;

class PlaylistController extends Controller
{
    public function displaySongs(){
        return DB::table('Playlists')->get();
    }

    public function store(Request $request){

    $newPlaylists = new Playlists();
    $newPlaylists->name = $request->name;
    $newPlaylists->save();
    return $newPlaylists;
}

    }
