<?php

use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('welcome');
    
});

Route::get('/songs', [SongsController::class, 'displaySongs']);
Route::post('/upload', [SongsController::class, 'store']);

Route::get('/playlist', [PlaylistsController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistsController::class, 'store']);

Route::get('/playlistsongs', [Playlist_SongsController::class, 'displayPlaylistSongs']);
Route::post('/create', [Playlist_SongsController::class, 'store']);
