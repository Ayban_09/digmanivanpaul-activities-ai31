<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlist__songs', function (Blueprint $table) {
            $table->id();
            //$table->songs::where('id', $request->id)->get();
            //$table->playlists::where('id', $request->id)->get();
            $table->integer('songs_id');
            $table->integer('playlist_id');            
            $table->timestamp('created_at');
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlist__songs');
    }
}
